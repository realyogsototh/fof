#!/usr/bin/env zsh

fullpath=$(readlink -f $0)
sdir=${fullpath:h}

source $sdir/test-functions.sh

urls=( 'broken-link'
    'Strange/behavior/index.html'
    'forgotten/old/link.html')
refs=( 'http://fof-demo.com/some/url'
       'http://fof-demo.com/Scratch/en/blog/bad-element/'
       ''
       'http://external.com/Some/Strange/External/url.html' )

for i in `seq 20`; do
    addFof "Y-5e78485899d9a89f938e" "http://fof-demo.com/${urls[$((1+RANDOM%4))]}" "${refs[$((1+RANDOM%5))]}"
done
