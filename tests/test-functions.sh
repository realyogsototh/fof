#!/usr/bin/env zsh

encodeURIComponent() {
print -n -- "$*" | perl -pe "s/([^0-9A-Za-z!\'()*\\-._~])/sprintf(\"%%%02X\", ord(\$1))/eg"
}
encodeURI() {
    print -n -- "$*" | perl -MURI::Escape -ne 'print uri_escape($_);'
}

get() {
    print -- curl -XGET $1
    curl -XGET $1
}

addFof() {
    local uid="$1"
    local url="$2"
    local ref="$3"
    get "http://localhost:3000/fof/$uid/$(encodeURI "$url")?referrer=$(encodeURIComponent "$ref")"
}
