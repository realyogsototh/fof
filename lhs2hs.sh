#!/usr/bin/env zsh


for fic in $@; do
print -n -- ${fic:r}.hs
perl -pe 's#^([^>])#-- $1#;s#^> ?##;s#^-- $##;s/^-- #(if|endif)/#$1/' < $fic > ${fic:r}.hs
print
done
