{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Site where

import           Import
import           Helpers.Handler
import           Yesod.Auth       (maybeAuthId)
import           Network.URI
import           Data.Maybe       (fromMaybe)
import qualified Data.Text  as T

actionForm :: Text -> FofId -> Maybe ActionId -> Widget
actionForm site_uid fof_id mActionId = do
    -- action from actionId
    maybeAction <- maybe (return Nothing)
                         (\actionId -> do
                            action <- handlerToWidget $ runDB $ get404 actionId
                            return $ return action) mActionId
    let typeSel x = mtest ((x ==).actionType) maybeAction
    let durationSel x = mtest ((Just x ==).actionDuration) maybeAction
    let urlStr = maybe "" (\a -> fromMaybe "" (actionUrl a)) maybeAction
    let commentStr = maybe "" (\a -> fromMaybe "" (actionComment a)) maybeAction

    $(widgetFile "actionForm")
    where
        mtest :: (a -> Bool) -> Maybe a -> Bool
        mtest _ Nothing  = False
        mtest g (Just x) = g x

-- homepage
getSiteR :: Text -> Handler TypedContent
getSiteR uid = do
  extra <- getExtra -- get settings.yml info
  let mainDomainName = extraMainDomain extra
  -- get the list of sites
  maybeSite <- runDB $ getBy $ UniqueSite uid
  Entity siteId site <- ensure maybeSite (invalidArgs ["bad site uid"])
  fofList <- runDB $ selectList [FofSite ==. siteId][Desc FofNbOcc, LimitTo 100]
  let cfofList = map (\(x,n) -> (x,T.pack("nb" ++ show n))) $ zip fofList ([0..] :: [Int])
  selectRep $ do
    provideRep $ defaultLayout $ do
        setTitle $ toHtml (siteDomainName site)
        $(widgetFile "stats")
    provideRep $ return $ array (map (\(Entity _ fof) -> fof) fofList)
  where
    shortref _ Nothing = ""
    shortref site (Just url) =
      (removePrefix domain) .
      (removePrefix "://") .
      (removePrefix "https") .
      (removePrefix "http") $ url
      where
        domain = siteDomainName site

-- Post
postSiteR :: Text -> Handler TypedContent
postSiteR _ = okPageJson "post SiteR"

-- Delete the entire list of entries
deleteSiteR :: Text -> Handler TypedContent
deleteSiteR uid = do
  maybeSite <- runDB $ getBy $ UniqueSite uid
  Entity siteId _ <- ensure maybeSite (invalidArgs ["bad site uid"])
  fofList <- runDB $ selectList [FofSite ==. siteId][]
  _ <- runDB $ do
            mapM_ (\(Entity fofId _) -> deleteWhere [ActionFofRef ==. fofId]) fofList
            deleteWhere [FofSite ==. siteId]
            delete siteId
  okPageJson "Site deleted"

postNewSiteR :: Handler RepHtml
postNewSiteR = do
  mUserId <- maybeAuthId
  userId <- ensure mUserId (permissionDenied "Please log in")
  runPost ["url"] (insertWebsite userId)
  where
    insertWebsite user [Just url] = do
      let mDomainName = domainNameFromURL (T.unpack url)
      domainName <- ensure mDomainName (invalidArgs ["url (bad format)"])
      insertSiteDomain user (T.pack domainName)
    insertWebsite _ _ = invalidArgs ["url"]
    insertSiteDomain user domainName = do
      siteId <- runDB $ insert $ Site (uidForDomain domainName) domainName user
      maybeSite <- runDB $ get siteId
      site <- ensure maybeSite notFound
      pc <- widgetToPageContent $(widgetFile "siteBloc")
      hamletToRepHtml [hamlet|^{pageBody pc}|]
    -- generate a nice uid for the domainName
    uidForDomain :: Text -> Text
    uidForDomain txt = "Y-" <> textHex (salt <> txt)
      where
        salt = "ce577c2a36f74c7abf656df0c094288392fec2d3c110aaea699e9f6916cacce2"
    -- accepts
    --  domainName.com
    --  foo://user:password@domainName.com/foo/bar
    -- returns domainName.com
    -- refuses
    --    domainName
    --    foo://user:password@domainName/foo/bar
    domainNameFromURL :: String -> Maybe String
    domainNameFromURL uri = do
      parsedURI <- case parseURI uri of
        Just parsed -> return parsed
        Nothing     -> parseURI ("http://" ++ uri)
      parsedAuth <- uriAuthority parsedURI
      let domainName = uriRegName parsedAuth
      case filter (=='.') domainName of
        [] -> Nothing
        _ -> return domainName

deleteFofesR :: Text -> Handler TypedContent
deleteFofesR uid = do
  maybeSite <- runDB $ getBy $ UniqueSite uid
  Entity siteId _ <- ensure maybeSite (invalidArgs ["bad site uid"])
  runDB $ deleteWhere [FofSite ==. siteId, FofAction ==. Nothing ]
  okPageJson "All 404 deleted"
