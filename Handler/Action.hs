module Handler.Action where

import Import
import Helpers.DB (dbMaybe)

getActionR :: Text -> FofId -> ActionId -> Handler TypedContent
getActionR _ _ actionId = do
    action <- runDB $ get404 actionId
    selectRep $ do
        provideRep $ defaultLayout [whamlet|
        <table>
            <tr>
                <td>Type
                <td>#{actionType action}
            $maybe comment <- actionComment action
                <tr>
                    <td>Comment
                    <td>#{comment}
            $maybe url <- (actionUrl action)
                <tr>
                    <td>URL
                    <td>#{url}
            $maybe duration <- actionDuration action
                <tr>
                    <td>Duration
                    <td>#{duration}
        |]
        provideRep $ return $ toJSON action

data PartialAction = PartialAction
    { partialActionType     :: Int
    , partialActionComment  :: Maybe Text
    , partialActionUrl      :: Maybe Text
    , partialActionDuration :: Maybe Int
    } deriving (Show)

postNewActionR :: Text -> FofId -> Handler TypedContent
postNewActionR _ fofId = do
    partialAction <- runInputPost $ PartialAction
                        <$> ireq intField   "type"
                        <*> iopt textField  "comment"
                        <*> iopt textField  "url"
                        <*> iopt intField   "duration"
    let action = Action (partialActionType        partialAction)
                        (partialActionComment     partialAction)
                        (partialActionUrl         partialAction)
                        (partialActionDuration    partialAction)
                        fofId
    runDB $ do
        actionId <- dbMaybe [ ActionFofRef ==. fofId ]
            (insert action)
            (\act -> do
                update act [ ActionType     =. actionType action
                           , ActionComment  =. actionComment action
                           , ActionUrl      =. actionUrl action
                           , ActionDuration =. actionDuration action ]
                return act)
        -- Just to be sure I do this everytime.
        -- Can be dependant of the index DB implementation.
        update fofId [FofAction =. Just actionId]
    selectRep $ do
      provideRep $ defaultLayout [whamlet|POST ACTION; TODO|]
      provideRep $ return $ toJSON action

deleteActionR :: Text -> FofId -> ActionId -> Handler TypedContent
deleteActionR _ _ actionId = do
    runDB $ delete actionId
    selectRep $ do
        provideRep $ defaultLayout [whamlet|DELETED ACTION; TODO|]
        provideRep $ return $ object ["msg" .= ("deleted" :: Text)]
