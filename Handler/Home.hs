-- The Home Handler
-- ----------------


{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Home where

import            Import
import            Helpers.Css
import            Helpers.Handler
import            Yesod.Auth
import            Data.Maybe         (isNothing)

-- The homepage


getHomeR :: Handler TypedContent
getHomeR = do
  mUserId <- maybeAuthId
  -- get the list of entires
  siteList <- case mUserId of
    Nothing -> return []
    Just user -> runDB $ selectList [SiteOwner ==. user][Desc SiteDomainName, LimitTo 100]
  -- display the list of entires
  selectRep $ do
    -- if html requested
    provideRep $ defaultLayout $ do
      setTitle $ if isNothing mUserId then "Welcome to fofanalytics!" else "My websites"
      $(widgetFile "homepage")
    -- if json requested
    provideRep $ return $ array (map (\(Entity _ (Site uid domainName _)) -> (uid,domainName)) siteList)
  where
    siteBloc :: Site -> Widget
    siteBloc site = $(widgetFile "siteBloc")
