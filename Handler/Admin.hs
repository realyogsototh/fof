module Handler.Admin where

import Import

getEntityId :: Entity a -> Key a
getEntityId (Entity uid _) = uid

getAdminR :: Handler Html
getAdminR = do
    userList <- runDB $ do
        users <- selectList ([] :: [Filter User])[LimitTo 100]
        nbSiteAndFofByUser <- flip mapM users $ \u -> do
                                sites <- selectList [SiteOwner ==. getEntityId u][]
                                nbSites <- count [SiteOwner ==. getEntityId u]
                                nbFofBySite <- flip mapM sites $ \s -> count [FofSite ==. getEntityId s]
                                let nbFofes = sum nbFofBySite
                                return $ nbFofes `seq` (nbSites,nbFofes)
        return $ zip users nbSiteAndFofByUser
    defaultLayout [whamlet|
        <table>
            <tr>
                <td>User
                <td>#Sites
                <td>#404
            $forall (Entity _ user, (nbSites, nbFofes)) <- userList
                <tr>
                    <td>#{userIdent user}
                    <td>#{nbSites}
                    <td>#{nbFofes}|]
