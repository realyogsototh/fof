-- The Fof Handler
-- ----------------

{-# LANGUAGE TupleSections, OverloadedStrings, StandaloneDeriving, TypeSynonymInstances, FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Handler.Fof
  ( deleteFofR
  , getReportFofR
  )
where

import            Import
import            Helpers.Handler
import            Helpers.DB                (updateOrCreate)
import            Text.Julius
import            Data.Maybe                (fromMaybe)
import            Yesod.Routes.Class        (Route)
import            Data.Text
import qualified  Data.Text.Lazy as TL
import            Data.Text.Lazy.Builder    (fromLazyText)
import qualified  Data.List as L

-- delete one fof entry

deleteFofR :: Text -> FofId -> Handler TypedContent
deleteFofR _ fofId = do
  runDB $ do
    update fofId [FofAction =. Nothing]
    deleteWhere [ActionFofRef ==. fofId]
    deleteWhere [ReferrerFofRef ==. fofId]
    delete fofId
  okPageJson "Deleted"

-- When a 404 is reported,
-- we send back some javascript to be executed on the client side.
getReportFofR :: Text -> Text -> Handler TypedContent
getReportFofR uid url = runGet ["referrer"] handleParam
  where
    handleParam [Just referrer] = insertOrUpdateFof (Just referrer)
    handleParam _               = insertOrUpdateFof Nothing
    -- Insert or update depending if this is the first time
    insertOrUpdateFof mRef = do
      mSiteId <- runDB $ getBy $ UniqueSite uid
      (Entity siteId (Site _ domain _)) <- ensure mSiteId (sendFile typeJavascript "static/js/bad.js")
      let maybeReferrer = if fromMaybe "" mRef == "" then Nothing else mRef
      let shortUrl = removePrefix domain url
      fofId <- updateOrCreate [FofSite ==. siteId, FofUrl ==. shortUrl]
                              [FofNbOcc +=. 1]
                              (Fof shortUrl 1 siteId Nothing)
      case maybeReferrer of
        Nothing -> return ()
        Just refUrl -> updateOrCreate  [ReferrerFofRef ==. fofId, ReferrerUrl ==. refUrl]
                                 [ReferrerNbOcc +=. 1]
                                 (Referrer refUrl 1 Nothing fofId)
                        >> return ()

      fof <- runDB $ get404 fofId
      let mActionId = fofAction fof
      mAction <- maybe (return Nothing) (fmap Just . runDB . get404) mActionId
      maybe (sendFile typeJavascript "static/js/ok.js") sendJavascriptFromAction mAction

-- Serve javascript

newtype RepJavascript = RepJavascript Content
instance ToTypedContent RepJavascript where
  toTypedContent (RepJavascript c) = TypedContent typeJavascript c
instance HasContentType RepJavascript where
  getContentType _ = typeJavascript
deriving instance ToContent RepJavascript
instance ToContent Javascript where
  toContent = toContent . renderJavascript
instance ToJavascript String where toJavascript = fromLazyText .TL.pack
-- instance ToJavascript Text where toJavascript = fromText

juliusToRepJavascript :: (ToContent a, MonadHandler m) => ((Route (HandlerSite m) -> [(Text, Text)] -> Text) -> a) -> m TypedContent
juliusToRepJavascript j =  do
  render <- getUrlRenderParams
  return $ toTypedContent $ RepJavascript (toContent $ j render)

sendJavascriptFromAction :: Action -> Handler TypedContent
sendJavascriptFromAction action = do
  alreadyExpired
  let typeA    = protectJSString $ show $ actionType action
  let url      = protectJSString $ unpack $ fromMaybe "" $ actionUrl action
  let comment  = protectJSString $ unpack $ fromMaybe "" $ actionComment action
  let duration = protectJSString $ fromMaybe "" $ fmap show $ actionDuration action
  juliusToRepJavascript $(juliusFile "templates/action.julius")
  where
      protectJSString :: String -> String
      protectJSString = L.concatMap protectChar
      protectChar :: Char -> String
      protectChar '"' = "\\\""
      protectChar '<' = "&lt;"
      protectChar '>' = "&gt;"
      protectChar '&' = "&amp;"
      protectChar '\n' = "<br/>"
      protectChar x = [x]
