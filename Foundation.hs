-- This is the `Foundation` module.
-- We start by importing a bunch of other modules.

module Foundation where

import Prelude
import Yesod
import Yesod.Static
import Yesod.Auth
import Yesod.Auth.BrowserId
import Yesod.Auth.GoogleEmail
import Yesod.Auth.OpenId
#if DEVELOPMENT
import Yesod.Auth.Dummy
#endif
import Yesod.Default.Config
import Yesod.Default.Util (addStaticContentExternal)
import Network.HTTP.Conduit (Manager)
import qualified Settings
import Settings.Development (development)
import qualified Database.Persist
import Database.Persist.Sql (SqlPersistT)
import Settings (widgetFile, Extra (..))
import Model
import Data.Text (Text)
import Text.Jasmine (minifym)
import Text.Hamlet (hamletFile)
import System.Log.FastLogger (Logger)

-- Needed for custom login handler

import qualified Yesod.Auth.Message as Msg

-- We also import some of our modules.

import Helpers.Css
import Helpers.DB

-- A kind of global variable, `App`. It is reachable in all our handlers.

data App = App
    { settings :: AppConfig DefaultEnv Extra
    , getStatic :: Static -- ^ Settings for static file serving.
    , connPool :: Database.Persist.PersistConfigPool Settings.PersistConf -- ^ Database connection pool.
    , httpManager :: Manager
    , persistConfig :: Settings.PersistConf
    , appLogger :: Logger
    }

-- Set up i18n messages. See the message folder.

mkMessage "App" "messages" "en"

-- This is where we define all of the routes in our application. For a full
-- explanation of the syntax, please see:
-- [http://www.yesodweb.com/book/handler](http://www.yesodweb.com/book/handler)

-- This function does three things:

-- - Creates the route datatype `AppRoute`. Every valid URL in your
--   application can be represented as a value of this type.
-- - Creates the associated type: `type instance Route App = AppRoute`
-- - Creates the value resourcesApp which contains information on the
--   resources declared below. This is used in Handler.hs by the call to
--   `mkYesodDispatch`.

-- What this function does *not* do is create a `YesodSite` instance for
-- `App`. Creating that instance requires all of the handler functions
-- for our application to be in scope. However, the handler functions
-- usually require access to the `AppRoute` datatype. Therefore, we
-- split these actions into two functions and place them in separate files.

mkYesodData "App" $(parseRoutesFile "config/routes")

-- A type alias to make it easier to write.

type Form x = Html -> MForm (HandlerT App IO) (FormResult x, Widget)

-- Please see the documentation for the Yesod typeclass. There are a number
-- of settings which can be configured by overriding methods here.

instance Yesod App where
    approot = ApprootMaster $ appRoot . settings

-- Store session data on the client in encrypted cookies,
-- default session idle timeout is 120 minutes

    makeSessionBackend _ = fmap Just $ defaultClientSessionBackend
        (120 * 60) -- 120 minutes
        "config/client_session_key.aes"

-- The general html layout.
-- We break up the default layout into two components:
-- default-layout is the contents of the body tag, and
-- default-layout-wrapper is the entire page. Since the final
-- value passed to hamletToRepHtml cannot be a widget, this allows
-- you to use normal widget features in default-layout.

    defaultLayout widget = do
        master <- getYesod
        mmsg <- getMessage
        muser <- maybeAuthId
        p <- widgetToPageContent $ do
            $(widgetFile "normalize")
            $(widgetFile "y")
            $(widgetFile "default-layout")
        hamletToRepHtml $(hamletFile "templates/default-layout-wrapper.hamlet")

-- Static content
-- ==============

-- This function creates static content files in the static folder
-- and names them based on a hash of their content. This allows
-- expiration dates to be set far in the future without worry of
-- users receiving stale content.

    addStaticContent =
        addStaticContentExternal minifym genFileName Settings.staticDir (StaticR . flip StaticRoute [])
      where
        -- Generate a unique filename based on the content itself
        genFileName lbs
            | development = "autogen-" ++ base64md5 lbs
            | otherwise   = base64md5 lbs

-- Place Javascript at bottom of the body tag so the rest of the page loads first

    jsLoader _ = BottomOfBody

-- What messages should be logged. The following includes all messages when
-- in development, and warnings and errors in production.

    shouldLog _ _source level =
        development || level == LevelWarn || level == LevelError

    makeLogger = return . appLogger

-- This is done to provide an optimization for serving static files from
-- a separate domain. Please see the staticRoot setting in Settings.hs

    urlRenderOverride y (StaticR s) =
        Just $ uncurry (joinPath y (Settings.staticRoot $ settings y)) $ renderRoute s
    urlRenderOverride _ _ = Nothing

-- The page to be redirected to when authentication is required.

    authRoute _ = Just $ AuthR LoginR

-- Authorisation rules
-- ===================

    isAuthorized (AuthR _) _ = return Authorized
    isAuthorized FaviconR _ = return Authorized
    isAuthorized RobotsR _ = return Authorized

-- Access to admin granted only for admin
    isAuthorized AdminR _ = isAdmin

-- Access to the homepage is always granted.

    isAuthorized HomeR _ = return Authorized

-- Creation of a site authorized only if logged

    isAuthorized NewSiteR _ = isLogged
    isAuthorized (NewActionR site _) _ = isSiteOwner site

-- on `SiteR` and `FofesR` (read and write),
-- authorize only for owner of the site.

    isAuthorized (SiteR site) _ = isSiteOwner site
    isAuthorized (FofesR site) _ = isSiteOwner site

-- on `ActionR` and `NewActionR` (read and write) only for owner of the action.

    isAuthorized (ActionR _ _ action) _ = isActionOwner action

-- The `FofR` ressource (delete a 404) is ok only if the fof belongs to the user.

    isAuthorized (FofR site_uid fofId) _ = do
        fof <- runDB $ get404 fofId
        site <- runDB $ get404 (fofSite fof)
        if site_uid == siteUid site
          then
              isSiteOwner site_uid
          else
              return $ Unauthorized "The site doesn't correspond"

-- Access to the 404 notification is always granted too.

    isAuthorized (ReportFofR _ _) _ = return $ Authorized

-- All other pages have restricted access by default.

    isAuthorized _ _ = return $ Unauthorized "Sorry. You can't access this page."

-- Function just testing the user is logged in.

isLogged :: Handler AuthResult
isLogged = do
      mu <- maybeAuthId
      case mu of
        Nothing -> return AuthenticationRequired
        _ -> return Authorized

isAdmin :: Handler AuthResult
isAdmin = do
      mu <- maybeAuthId
      case mu of
        Nothing   -> return AuthenticationRequired
        Just userId -> do
            (Entity _ user:_) <- runDB $ selectList [UserId ==. userId][LimitTo 1]
            case userIdent user of
                "yann.esposito@gmail.com" -> return Authorized
#if DEVELOPMENT
                "admin"                   -> return Authorized
#endif
                _                         -> return $ Unauthorized "You must be an admin"
-- The function to verify the currently logged user own the site which
-- the UID is given in parameter.

isSiteOwner :: Text -> Handler AuthResult
isSiteOwner site = do
      mu <- maybeAuthId
      case mu of
        Nothing -> return AuthenticationRequired
        Just user -> do
            auth <- runDB $ dbMaybe [SiteUid ==. site,SiteOwner ==. user]
                              (return $ Unauthorized "You don't own this site.")
                              (\_->return Authorized)
            return auth

isActionOwner :: ActionId -> Handler AuthResult
isActionOwner actionId = do
      mu <- maybeAuthId
      case mu of
        Nothing -> return AuthenticationRequired
        Just user -> do
            action <- runDB $ get404 actionId
            fof <- runDB $ get404 (actionFofRef action)
            site <- runDB $ get404 (fofSite fof)
            if (siteOwner site == user)
              then return $ Unauthorized "You don't own this site."
              else return $ Authorized

-- How to run database actions.
-- ============================

instance YesodPersist App where
    type YesodPersistBackend App = SqlPersistT
    runDB = defaultRunDB persistConfig connPool
instance YesodPersistRunner App where
    getDBRunner = defaultGetDBRunner connPool

-- Authentification
-- ================

instance YesodAuth App where
    type AuthId App = UserId

    -- Where to send a user after successful login
    loginDest _ = HomeR
    -- Where to send a user after logout
    logoutDest _ = HomeR

    -- Custom login page
    loginHandler = do
      tp <- getRouteToParent
      lift $ loginLayout $ do
          setTitleI Msg.LoginTitle
          master <- getYesod
          mapM_ (flip apLogin tp) (authPlugins master)
      where
          loginLayout widget = do
              master <- getYesod
              mmsg <- getMessage
              muser <- maybeAuthId
              p <- widgetToPageContent $ do
                  $(widgetFile "normalize")
                  $(widgetFile "y")
                  $(widgetFile "login")
              hamletToRepHtml $(hamletFile "templates/default-layout-wrapper.hamlet")

    getAuthId creds = runDB $ do
        x <- getBy $ UniqueUser $ credsIdent creds
        case x of
            Just (Entity uid _) -> return $ Just uid
            Nothing -> do
                fmap Just $ insert $ newUser (credsIdent creds)
        where
          newUser ident = User ident Nothing Nothing

    -- You can add other plugins like BrowserID, email or OAuth here
    authPlugins _ = [ authBrowserId def
                    , authGoogleEmail
                    , authOpenId Claimed []
#if DEVELOPMENT
                    , authDummy
#endif
                    ]

    authHttpManager = httpManager

-- This instance is required to use forms. You can modify renderMessage to
-- achieve customized and internationalized form validation messages.

instance RenderMessage App FormMessage where
    renderMessage _ _ = defaultFormMessage

-- Get the 'Extra' value, used to hold data from the settings.yml file.

getExtra :: Handler Extra
getExtra = fmap (appExtra . settings) getYesod

-- Note: previous versions of the scaffolding included a deliver function to
-- send emails. Unfortunately, there are too many different options for us to
-- give a reasonable default. Instead, the information is available on the
-- wiki:

-- [https://github.com/yesodweb/yesod/wiki/Sending-email](https://github.com/yesodweb/yesod/wiki/Sending-email)
