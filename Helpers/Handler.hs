module Helpers.Handler
  ( errorPage
  , errorPageJson
  , okPage
  , okPageJson
  , showId
  , delWidget
  , deleteWidget
  , runGet
  , runPost
  , ensure
  , removePrefix
  , textHex
  , textHash
  , mget
  )
where

import            Prelude
import            Import
import qualified  Data.Text                     as T
import Yesod.Core

import Crypto.Hash.SHA256
import qualified Data.ByteString.Char8 as B
import Data.ByteString.Base64 (encode)
import Text.Printf (printf)


-- Show the id number from an entry
-- Can be used inside HTML
showId :: Key a -> String
showId k = showInt64 $ unKey k
  where showInt64 (PersistInt64 i) = show i
        showInt64 _ = "unknow"

errorPage :: Text -> Handler RepHtml
errorPage errorText = do
  let errorMessage = toHtml errorText
  defaultLayout $ do
    setTitle errorMessage
    [whamlet| <p>#{errorMessage}|]

okPage :: Text -> Handler RepHtml
okPage = errorPage

errorPageJson :: Text -> Handler TypedContent
errorPageJson errorText = do
  selectRep $ do
    provideRep $ defaultLayout $ do
      setTitle errorMessage
      [whamlet| <p>#{errorMessage}|]
    provideRep $ return $ object ["msg" .= errorText]
  where
    errorMessage = toHtml errorText

okPageJson :: Text -> Handler TypedContent
okPageJson = errorPageJson

delWidget :: Route App -> Widget
delWidget deleteURL = deleteWidget "destroy" "delete" "cancel" "destroy" deleteURL

deleteWidget :: Text -> Text -> Text -> Text -> Route App -> Widget
deleteWidget actionName actionText cancelText triggerActionText deleteURL =
  let x=( actionName :: Text
        , actionText :: Text
        , cancelText :: Text
        , triggerActionText :: Text )
  in do
    render <- getUrlRender
    deleteActionUID <- return $ "id-" <> (textHex $ render deleteURL)
    $(widgetFile "delete")

textHex :: Text -> Text
textHex = T.pack . take 20 . toHex . hash . B.pack . T.unpack
      where
        toHex str = B.unpack str >>= printf "%02x"
textHash :: Text -> Text
textHash = T.pack . B.unpack . B.take 20 . encode . hash . B.pack . T.unpack

-- use a low level parameter access
runGet :: [Text] -> ([Maybe Text] -> Handler a) -> Handler a
runGet keys handler = do
  values <- mapM lookupGetParam keys
  handler values

-- use a low level parameter access
runPost :: [Text] -> ([Maybe Text] -> Handler a) -> Handler a
runPost keys handler = do
  values <- mapM lookupPostParam keys
  handler values

ensure :: Maybe a -> Handler a -> Handler a
ensure maybeValue handlerOnError =
  case maybeValue of
    Nothing -> handlerOnError
    Just v  -> return v

-- removePrefix "prefix" "prefixTheText" = "TheText"
removePrefix :: Text -> Text -> Text
removePrefix txt input = if T.isPrefixOf txt input
                         then T.drop (T.length txt) input
                         else input

-- get using a maybe function with error
mget :: Handler (Maybe a) -> Handler a -> Handler a
mget fToMaybe errorFunc = do
  mObj <- fToMaybe
  obj <- ensure mObj errorFunc
  return obj
