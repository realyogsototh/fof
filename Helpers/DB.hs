-- | This module provide some useful tools to works with persistent
module Helpers.DB
  ( getMaybe, dbMaybe, updateOrCreate)
where

import Prelude
import Yesod

-- |`getMaybe` try to select one element
-- return Nothing if there is no entry
-- return Just ob if ob is the first selected object
getMaybe :: (PersistEntity val, PersistQuery m,
             PersistEntityBackend val ~ PersistMonadBackend m) =>
            [Filter val]            -- ^ select filter
            -> m (Maybe (Key val))
getMaybe selectFilter = do
  res <- selectList selectFilter [LimitTo 1]
  case res of
    []                      -> return Nothing
    ((Entity objectId _):_) -> return (Just objectId)

-- |the `maybe` function extended to persistent
-- Search for an element and perform an action on it if it exists
-- or another action otherwise.
--
-- typical usage:
--
-- > dbMaybe [ObjectIdentifier ==. objId]                   -- search
-- >         (insert newObject >> return ())                -- not found action
-- >         (\ob -> update ob [ObjectAccessCounter +=. 1]) -- found action
dbMaybe ::(PersistEntity val, PersistQuery m,
           PersistEntityBackend val ~ PersistMonadBackend m) =>
          [Filter val]        -- ^ select filter to search db entry
          -> m b              -- ^ default action
          -> (Key val -> m b) -- ^ action taking an objectId as parameter
          -> m b
dbMaybe selectFilter actionIfNotFound actionIfFound = do
  maybeObjectId <- getMaybe selectFilter
  case maybeObjectId of
    Nothing -> actionIfNotFound
    Just objectId -> actionIfFound objectId

-- | `updateOrCreate` search for a field and update it.
-- if the field isn't in the DB, then a new field is created.
--
-- example:
-- > updateOrCreate [UserName ==. "Toby"] [UserFriendsNumber +=. 1] (User "Toby" 1)
updateOrCreate :: (YesodPersist site,
                   PersistEntity val,
                   PersistQuery (YesodPersistBackend site (HandlerT site IO)),
                   PersistMonadBackend (YesodPersistBackend site (HandlerT site IO))
                   ~ PersistEntityBackend val) =>
                   [Filter val]     -- ^ The search filter(s)
                   -> [Update val]  -- ^ The update command
                   -> val           -- ^ The value to insert in case of no match
                   -> HandlerT site IO (KeyBackend (PersistEntityBackend val) val)
updateOrCreate search updater defaultValue = runDB $ do
    mref <- selectList search [LimitTo 1]
    case mref of
        []                  -> insert defaultValue
        (Entity refid _:_)  -> update refid updater >> return refid
