Default: &defaults
  user: fof
  password: fof
  host: localhost
  port: 5432
  database: fof
  poolsize: 10

Development:
  <<: *defaults

Testing:
  database: fof_test
  <<: *defaults

Staging:
  database: fof_staging
  poolsize: 100
  <<: *defaults

Production:
  poolsize: 100
